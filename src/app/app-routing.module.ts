import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomePageComponent } from "./components/home-page/home-page.component";
import { ItemListComponent } from "./components/item-list/item-list.component";
import { LoginComponent } from "./components/login/login.component";
import { AuthGuard } from "./auth.guard";

const routes: Routes = [
    {path: '', component: HomePageComponent,},
    {path: 'login', component: LoginComponent,},
    {path: 'devices', component: ItemListComponent, canActivate:[AuthGuard]},
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})export class AppRoutingModule{

}