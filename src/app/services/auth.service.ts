import { Injectable } from '@angular/core';
import { User } from './../interfaces/insterfases';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


export interface AuthResponseData{
  msg: String,
  data: {
    msg: String,
    data: {
      access_token: String,
      token_type: "Bearer",
      expires_at: Number
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuth = false;  

  constructor(private http: HttpClient, private router: Router) { }

  login(user: User): Observable<AuthResponseData> {
    const candidate = {
      "email": user.email,
      "password": user.password,
      "personal_data_access": true
    }
    return this.http.post<AuthResponseData>('https://core.nekta.cloud/api/auth/login', candidate)
  }

  authSuccessfull(){
    this.isAuth = true;
  }
  logout() {
    localStorage.setItem('access_token', "");
    this.router.navigate(['/']);
  }
  isAuthenticated() {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(this.isAuth);
      }, 0)
    })
  }

  getToken(){
    return localStorage.getItem('access_token');
  }

}
