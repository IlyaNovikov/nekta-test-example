import { Component, OnInit, OnDestroy } from '@angular/core';
import { DevicesService } from './../../services/devices.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.less']
})
export class ItemListComponent implements OnInit, OnDestroy {
  loading = true;
  devices: any[] = [];
  aSub: Subscription | any;
  constructor(private devicesService: DevicesService, private authService:AuthService) { }

  ngOnInit() {
    this.aSub = this.devicesService.getDevices()
      .subscribe((response: any) => {
        this.devices = response.data.metering_devices.data
        this.loading = false;
      }, error => {
        console.log('error')
        this.loading = false;
      });
  }

  logout(){
    this.authService.logout()
  }

  ngOnDestroy(): void {
    this.aSub.unsubscribe();
  }
}
