import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { User } from './../../interfaces/insterfases';
import { Subscription } from 'rxjs';

interface Token {
  access_token: string,
  token_type: string
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit, OnDestroy {
  aSub: Subscription | any;
  form: FormGroup | any;
  isLoading = false;
  error: String = "";
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('access_token')) {
      this.router.navigate(['/devices'])
    }
    this.form = new FormGroup({
      email: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl('', [Validators.minLength(4), Validators.required]),
    });
  }
  submit() {
    this.isLoading = true;
    const candidate: User = {
      email: this.form.value.email,
      password: this.form.value.password,
    }

   this.aSub = this.authService.login(candidate).subscribe((response: | any) => {
      const data = response.data;
      this.isLoading = false;
      localStorage.setItem('access_token', data.access_token)
      this.router.navigate(['/devices'])
      this.authService.authSuccessfull();
    }, error => {
      this.isLoading = false;
      this.error = "Имя пользователя или пароль введены не верно!";
    });

  }

  ngOnDestroy(): void {
    this.aSub.unsubscribe();
  }
}
