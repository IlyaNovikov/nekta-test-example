import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service'; import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }
  login() {
    this.router.navigate(['/login'])
  }
  goHome() {
    this.router.navigate(['/'])
  }

}
