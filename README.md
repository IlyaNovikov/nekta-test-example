# Nekta-Test-example

The application consists of 3 pages: 1 Home Page, on page 2 there is a registration form with validation:

![image.png](./image.png)

After successful registration, we get to the device list page, which displays the id, name and last_active:

![image-1.png](./image-1.png)

